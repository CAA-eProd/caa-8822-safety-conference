var schoolApp = angular.module('schoolApp', ['ngAnimate']);

schoolApp.controller('NavController', function($scope) {

  $scope.toggleNav = false;

  window.addEventListener("resize", function() {
    if (window.matchMedia("(min-width: 768px)").matches) {
        $scope.toggleNav = false;
        $scope.$digest();
    }
  });


   $scope.toggleLight = false;

});

schoolApp.directive('countdown', [
'Util',
'$interval',
function (Util, $interval) {
    return {
        restrict: 'A',
        scope: { date: '@' },
        link: function (scope, element) {
            var future;
            var diff;
            future = new Date(scope.date);
            promise = $interval(function () {
                diff = Math.floor((future.getTime() - new Date().getTime()) / 1000);
                return element.html(Util.dhms(diff));
            }, 1000);

            if (element.html(Util.dhms(diff)) == 'done') {
             $interval.cancel(promise);
            }
        }
    };
}
]).factory('Util', [function () {
    return {
        dhms: function (t) {
            var days, hours, minutes, seconds;
            days = Math.floor(t / 86400);
            t -= days * 86400;
            hours = Math.floor(t / 3600) % 24;
            t -= hours * 3600;
            minutes = Math.floor(t / 60) % 60;
            t -= minutes * 60;
            seconds = t % 60;

            if (days <= 0 && hours <= 0 && minutes <= 0 && seconds <= 0 || days < 0) { return 'done';  } else {
              return [
                '<span class="countdown__item countdown--days"><div class="countdown__number">' +  days + '</div><div class="countdown__copy">Days</div></span>',
                '<span class="countdown__item countdown--hours"><div class="countdown__number">' +  hours + '</div><div class="countdown__copy">Hours</div></span>',
                '<span class="countdown__item countdown--minutes"><div class="countdown__number">' +  minutes + '</div><div class="countdown__copy">Minutes</div></span>',
                '<span class="countdown__item countdown--seconds"><div class="countdown__number">' +  seconds + '</div><div class="countdown__copy">Seconds</div></span>',
              ].join(' ');
            }


        }
    };
}]);


schoolApp.controller('FormController', function($scope) {
  $scope.subscribe = true;

  $scope.submitForm = function() {

    var firstNameInput = document.getElementById("first-name").value;
    var lastNameInput = document.getElementById("last-name").value;
    var emailInput = document.getElementById("email-address").value;

    if (firstNameInput == '' || firstNameInput == null) {
        $scope.subscribeForm['First Name'].$setDirty();
        $scope.subscribeForm['First Name'].$setTouched();
    } else if (lastNameInput == '' || lastNameInput == null) {
        $scope.subscribeForm['Last Name'].$setDirty();
        $scope.subscribeForm['Last Name'].$setTouched();
    } else if (emailInput == '' || emailInput == null || $scope.subscribeForm['Email Address'].$invalid) {
        $scope.subscribeForm['Email Address'].$setDirty();
        $scope.subscribeForm['Email Address'].$setTouched();
    } else {
      document.getElementById("form1").submit();
    }
  };
});

schoolApp.controller('DirectionsController', function($scope, $location) {
  $scope.dropMenu = false;

  $scope.options = [{
      title: 'Toronto Pearson International Airport',
      url: 'one.tpl.html'
    }, {
      title: 'Billy Bishop',
      url: 'two.tpl.html'
    }, {
      title: 'Transit',
      url: 'three.tpl.html'
  }];
  $scope.currentOption = 'one.tpl.html';

  $scope.onClickOption = function (option) {
    $scope.currentOption = option.url;
    $scope.dropMenu = false;
    $scope.currentItem = $scope.currentOption;
  };

  $scope.isActiveOption = function(optionUrl) {
    return optionUrl == $scope.currentOption;
  };

  $scope.currentItem = 'one.tpl.html';
});

schoolApp.controller('TransportationController', function($scope, $location) {

  $scope.tabs = [{
      title: 'Transit',
      url: 'transit.tpl.html'
    }, {
      title: 'By Car',
      url: 'car.tpl.html'
    }, {
      title: 'By Taxi',
      url: 'taxi.tpl.html'
  }];

  $scope.currentTab = 'transit.tpl.html';

  $scope.onClickTab = function (tab) {
    $scope.currentTab = tab.url;
  };

  $scope.isActiveTab = function(tabUrl) {
    return tabUrl == $scope.currentTab;
  };

});

schoolApp.controller('SponsorshipController', function($scope, $location) {

  $scope.tabs = [{
      title: 'Gold',
      url: 'gold.tpl.html'
    }, {
      title: 'Silver',
      url: 'silver.tpl.html'
    }, {
      title: 'Bronze',
      url: 'bronze.tpl.html'
  }];

  $scope.currentTab = 'gold.tpl.html';

  $scope.onClickTab = function (tab) {
    $scope.currentTab = tab.url;
  };

  $scope.isActiveTab = function(tabUrl) {
    return tabUrl == $scope.currentTab;
  };

});