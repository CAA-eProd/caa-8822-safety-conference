// Dependencies
var gulp = require('gulp'),
sass = require('gulp-ruby-sass'),
mmq = require('gulp-merge-media-queries'),
cleanCSS = require('gulp-clean-css'),
uglify = require('gulp-uglify'),
concat = require("gulp-concat"),
htmlmin = require('gulp-htmlmin'),
notify = require('gulp-notify'),
imagemin = require('gulp-imagemin'),
browserSync = require('browser-sync').create();




// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
});

// Compile Our Sass
gulp.task('styles', function() {
    return sass('./src/sass/*.scss', { style: 'expanded' })
        .pipe(gulp.dest('./src/sass/toMerge/'))
        .pipe(browserSync.stream())
        .pipe(notify({ message: 'Styles task complete' }));
});

// Merge Media Queries (Requires 2 media queries)
gulp.task('mmq', function () {
    return gulp.src('./src/sass/toMerge/*.css')
    .pipe(mmq({
      log: true
    }))
    .pipe(gulp.dest('./src/css/'))
    .pipe(browserSync.stream())
    .pipe(notify({ message: 'Merge complete' }));
});

// Minify CSS
gulp.task('minify-css', function() {
  return gulp.src('./src/css/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./dist/css/'))
    .pipe(browserSync.stream())
    .pipe(notify({ message: 'CSS Minification complete' }));
});

//Concat
gulp.task("concat", function () {
    return gulp.src(["./src/js/*.js"])
        .pipe(concat("app.js"))
        .pipe(gulp.dest("./src/js/toUglify/"))
        .pipe(browserSync.stream())
        .pipe(notify({ message: 'JS concat complete' }));
});

// Uglify JS
gulp.task('uglify-js', function() {
    return gulp.src('./src/js/toUglify/*.js')
        .pipe(uglify({mangle: false}))
        .pipe(gulp.dest('./dist/js/'))
        .pipe(browserSync.stream())
        .pipe(notify({ message: 'JS Uglification complete' }));
});

//Minify HTML
gulp.task('minify-html', function() {
  return gulp.src('./src/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./dist/'))
    .pipe(browserSync.stream())
    .pipe(notify({ message: 'HTML Minification complete' }));
});

//Optimize images
gulp.task('minify-image', function() {
  return gulp.src('src/img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./dist/img'))
    .pipe(browserSync.stream())
    .pipe(notify({ message: 'Image Minification complete' }));
});

// Watch Files For Changes
gulp.task('watch', function() {
  gulp.watch('src/**/*.scss', ['styles'])
  gulp.watch('src/**/*.css', ['mmq'])
  gulp.watch('src/**/*.css', ['minify-css'])
  gulp.watch('src/**/*.js', ['concat'])
  gulp.watch('src/**/*.js', ['uglify-js'])
  gulp.watch('src/**/*.html', ['minify-html'])
  gulp.watch(['src/**/*.png', 'src/**/*.jpg'], ['minify-image'])
  gulp.watch(['dist/**']).on('change', browserSync.reload);
});

// Run tasks
gulp.task('default', [ 'browser-sync','styles','mmq','minify-css','concat','uglify-js','minify-html','minify-image','watch']);