<div>&nbsp;</div>

<div><span class="token_content" contenteditable="false" rel="_form_is_complete_date_56728_1">Form completion date</span></div>

<div>&nbsp;</div>

<div>Dear&nbsp;<span class="token_content" contenteditable="false" rel="first_name_1">First Name</span>,</div>

<div><br />
Thank you for registering for the Ontario School Zone Safety Conference.&nbsp;</div>

<div>&nbsp;</div>

<div>Your payment has been processed, and the full details are below:</div>

<div><br />
Name: <span class="token_content" contenteditable="false" rel="first_name_1">First Name</span>&nbsp;<span class="token_content" contenteditable="false" rel="last_name_1">Last Name</span></div>

<div>Organization: <span class="token_content" contenteditable="false" rel="f_56745_300100_1">Organization</span>&nbsp;</div>

<div>&nbsp;</div>

<div>Last payment: $<span class="token_content" contenteditable="false" rel="_payment_last_payment_1">Last payment</span></div>

<div>&nbsp;</div>

<div><span class="token_content" contenteditable="false" rel="_order_cart_1">Cart list</span></div>

<div>&nbsp;</div>

<div><br />
The School Zone Safety Conference takes place at the Chelsea Hotel in Toronto on October 2, 2017. A special delegate rate of $159/night* has been made available to attendees, but rooms are limited.<br />
&nbsp;<br />
To receive the reduced rate you must reserve your room prior to the cut-off date of September 7, 2017.<br />
&nbsp;<br />
To book online, please <a href="https://gc.synxis.com/rez.aspx?Hotel=59052&amp;Chain=10316&amp;start=availresults&amp;arrive=10/1/2017&amp;depart=10/2/2017&amp;adult=1&amp;child=0&amp;group=CAA10117" target="_blank">click here</a>&nbsp;<br />
&nbsp;<br />
To book by phone:<br />
&nbsp;<br />
Use the toll free 1-800-CHELSEA (243-5732) or 416-595-1975 and identify yourself as part of the &ldquo;Canadian Automobile Association&rdquo; October conference.<br />
&nbsp;<br />
If you have any concerns or questions, please email <a href="mailto:trafficsafety@caasco.ca" target="_blank">trafficsafety@caasco.ca</a>.<br />
&nbsp;<br />
We look forward to seeing you in October!<br />
&nbsp;<br />
Thank you.<br />
&nbsp;<br />
*Rate applies to bookings before cut-off date and hotel availability.&nbsp; This is the single and double occupancy, check with hotel for triple or quad discounted occupancy rate.<br />
&nbsp;</div>
