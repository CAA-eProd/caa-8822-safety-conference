(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [
		{name:"728x90__display_ad_atlas_", frames: [[0,184,110,40],[0,92,201,90],[112,184,100,38],[0,0,201,90]]}
];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



(lib.Bitmap5 = function() {
	this.spriteSheet = ss["728x90__display_ad_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.familypool = function() {
	this.spriteSheet = ss["728x90__display_ad_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.logo = function() {
	this.spriteSheet = ss["728x90__display_ad_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.mancallcar = function() {
	this.spriteSheet = ss["728x90__display_ad_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.txtgood = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00338D").s().p("AgMANIAAgZIAZAAIAAAZg");
	this.shape.setTransform(236.3,23.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00338D").s().p("AgOBWIgMgEQgKgFgHgKQgHgIgEgMQgEgLAAgOQAAgNAEgLQADgLAHgKQAIgIAKgFQALgGAOAAIAJABIALADQAGADAFAEQAFADADAGIABAAIAAhAIAUAAIAACqIgUAAIAAgRIgBAAQgFALgLAFQgKAEgNAAgAgQgUQgHAFgFAHQgEAGgCAIQgCAJAAAIQAAAJACAIQADAJAEAGQAFAHAHADQAIAEAIAAQALAAAHgEQAHgEAFgGQAEgHACgJQACgIAAgJQAAgIgCgJQgCgHgFgHQgEgGgIgFQgHgEgLAAQgJAAgHAEg");
	this.shape_1.setTransform(226.5,16.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00338D").s().p("AgNBAIgMgEQgLgFgIgJQgHgIgEgMQgEgMAAgOQAAgMAEgMQAEgMAHgJQAIgJALgFQAMgFANAAQAPAAALAFQALAFAIAJQAIAJAEAMQADAMAAAMQAAAOgDAMQgEAMgIAIQgIAJgLAFIgMAEIgOABgAgOgrQgHADgFAHQgGAGgCAIQgEAJAAAKQAAAMAEAIQACAJAGAGQAFAGAHADQAHADAHAAQAIAAAHgDQAHgDAFgGQAGgGADgJQADgIAAgMQAAgKgDgJQgDgIgGgGQgFgHgHgDQgHgDgIAAQgHAAgHADg");
	this.shape_2.setTransform(213.9,18.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00338D").s().p("AgNBAIgMgEQgLgFgIgJQgHgIgEgMQgEgMAAgOQAAgMAEgMQAEgMAHgJQAIgJALgFQAMgFANAAQAPAAALAFQALAFAIAJQAIAJAEAMQADAMAAAMQAAAOgDAMQgEAMgIAIQgIAJgLAFIgMAEIgOABgAgOgrQgHADgFAHQgGAGgCAIQgEAJAAAKQAAAMAEAIQACAJAGAGQAFAGAHADQAHADAHAAQAIAAAHgDQAHgDAFgGQAGgGADgJQADgIAAgMQAAgKgDgJQgDgIgGgGQgFgHgHgDQgHgDgIAAQgHAAgHADg");
	this.shape_3.setTransform(201.2,18.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#00338D").s().p("AgSBXQgIgCgHgEQgIgFgFgHQgFgGAAgLIAUAAQABAGADAEQADAEAEACIALAEIAKABQAJgBAHgDQAIgEAEgGQAFgGABgIQADgJAAgKIAAgIQgGALgLAGQgKAEgMAAQgOAAgJgEQgLgGgHgIQgHgJgDgLQgEgKAAgNQAAgLADgLQADgMAHgKQAGgJAMgGQAKgHAQAAQALAAAKAGQAKAFAGAKIAAgSIATAAIAABxQAAAPgEAMQgDALgHAIQgOAPgdAAIgSgCgAgOhCQgIAEgDAGQgFAHgCAHQgCAIAAAJQAAAJACAJQABAHAEAHQAEAGAHAFQAHAEAJAAQAKAAAHgEQAHgFAFgGQADgHACgHQACgKABgIIgCgQQgCgIgEgGQgFgGgGgFQgHgEgJAAQgKAAgGAEg");
	this.shape_4.setTransform(188.1,21);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#00338D").s().p("AgTA/QgJgCgHgFQgIgFgEgIQgEgIgBgMIAVAAQAAAHADAEQADAFAEADQAFADAGABIALABIAKgBIAKgCQAEgCADgEQADgEAAgFQAAgIgGgEQgGgEgJgDIgTgEIgTgGQgJgDgGgHQgGgGAAgMQAAgKAEgHQAEgGAHgEQAGgFAJgCQAIgCAIAAQAKAAAJACQAIACAHAEQAHAFAEAHQAEAIABALIgUAAQgBgGgCgEQgDgEgEgCQgEgDgFgBIgJgBIgJABIgJADQgEABgCADQgDADAAAFQAAAFAEAEQAEADAGADIANADIANAEIAPAEQAHACAGADQAGAEADAGQAEAGAAAJQAAALgFAIQgFAHgHAFQgIAEgJACIgTACQgKAAgJgCg");
	this.shape_5.setTransform(170.9,18.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#00338D").s().p("AgmBWIgHgBIAAgTIAGACIAGABQAGAAAFgDQAEgDACgGIAIgUIgxh7IAXAAIAjBlIAAAAIAjhlIAWAAIg2CMIgGAPQgEAGgEAEQgEAEgFACQgFACgHAAg");
	this.shape_6.setTransform(159.9,21.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#00338D").s().p("AAfA8QgEgEgBgKIgJAJQgEADgGACQgLAFgNAAQgHAAgIgCQgIgCgGgEQgFgEgDgHQgEgGABgKQgBgKAEgHQAEgHAFgEQAHgDAHgCIAPgEIAQgCIANgDQAGgBADgDQADgDAAgGQABgHgDgEQgDgEgEgCIgJgDIgIgBQgNAAgJAFQgJAFAAAOIgVAAQABgMAFgIQADgIAIgFQAHgEAJgDQAJgCALAAIAQACQAIABAHADQAHAEAEAHQADAHAAAKIAAA/IABALQACADAEAAIAHgBIAAAQQgGADgKAAQgHAAgFgFgAAPACIgNACIgNACIgMAEQgGACgDAFQgEAEAAAIQAAAFACADIAGAGQADACAEABIAKABQAJAAAHgDQAHgDAEgEQAFgEACgFQACgFAAgEIAAgUQgEACgGABg");
	this.shape_7.setTransform(148.8,18.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#00338D").s().p("AgOBWIgMgEQgKgFgHgKQgHgIgEgMQgEgLAAgOQAAgNAEgLQADgLAHgKQAIgIAKgFQALgGAOAAIAJABIALADQAGADAFAEQAFADADAGIABAAIAAhAIAUAAIAACqIgUAAIAAgRIgBAAQgFALgLAFQgKAEgNAAgAgQgUQgHAFgFAHQgEAGgCAIQgCAJAAAIQAAAJACAIQADAJAEAGQAFAHAHADQAIAEAIAAQALAAAHgEQAHgEAFgGQAEgHACgJQACgIAAgJQAAgIgCgJQgCgHgFgHQgEgGgIgFQgHgEgLAAQgJAAgHAEg");
	this.shape_8.setTransform(135.6,16.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#00338D").s().p("AgOBWIgMgEQgKgFgHgKQgHgIgEgMQgEgLAAgOQAAgNAEgLQADgLAHgKQAIgIAKgFQALgGAOAAIAJABIALADQAGADAFAEQAFADADAGIABAAIAAhAIAUAAIAACqIgUAAIAAgRIgBAAQgFALgLAFQgKAEgNAAgAgQgUQgHAFgFAHQgEAGgCAIQgCAJAAAIQAAAJACAIQADAJAEAGQAFAHAHADQAIAEAIAAQALAAAHgEQAHgEAFgGQAEgHACgJQACgIAAgJQAAgIgCgJQgCgHgFgHQgEgGgIgFQgHgEgLAAQgJAAgHAEg");
	this.shape_9.setTransform(116.7,16.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#00338D").s().p("AAfA8QgFgEAAgKIgJAJQgFADgFACQgKAFgNAAQgIAAgIgCQgIgCgGgEQgFgEgDgHQgDgGAAgKQAAgKADgHQAEgHAFgEQAHgDAHgCIAQgEIAPgCIANgDQAGgBADgDQAEgDgBgGQAAgHgCgEQgDgEgEgCIgJgDIgIgBQgNAAgJAFQgIAFgBAOIgUAAQAAgMAEgIQAEgIAIgFQAHgEAJgDQAJgCALAAIAPACQAJABAHADQAGAEAEAHQAEAHABAKIAAA/IABALQAAADAFAAIAGgBIAAAQQgFADgKAAQgHAAgFgFgAAPACIgNACIgNACIgMAEQgGACgDAFQgEAEAAAIQAAAFACADIAFAGQAEACAEABIAJABQAKAAAGgDQAIgDAEgEQAFgEACgFQACgFAAgEIAAgUQgDACgHABg");
	this.shape_10.setTransform(104.9,18.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#00338D").s().p("AgHBWIgLgDQgGgCgFgEQgEgDgEgGIAAAAIAAAQIgVAAIAAiqIAVAAIAABAIAAAAQADgFAEgEQAEgDAFgCQALgGAMAAIANACQAHABAFADQALAFAHAJQAHAJADAMQAEALAAANQAAANgEAMQgDALgHAJQgHAJgLAFQgLAFgOAAgAgSgUQgHAFgEAHQgFAGgCAHQgCAJAAAIQAAAKACAIQACAIAFAHQAFAHAHADQAIAEAJAAQALAAAHgEQAHgEAEgGQAFgHABgJQACgIAAgKQAAgIgCgJQgCgHgFgGQgFgGgHgFQgHgEgJAAQgKAAgIAEg");
	this.shape_11.setTransform(92.4,16.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#00338D").s().p("AgRBXQgKgCgGgEQgIgFgEgHQgGgGAAgLIAUAAQABAGADAEQADAEAEACIALAEIAKABQAKgBAHgDQAGgEAFgGQAFgGACgIQACgJAAgKIAAgIQgGALgLAGQgLAEgKAAQgOAAgKgEQgLgGgHgIQgHgJgDgLQgEgKAAgNQAAgLADgLQADgMAHgKQAGgJAMgGQALgHAPAAQAMAAAJAGQAKAFAGAKIAAgSIATAAIAABxQAAAPgDAMQgEALgHAIQgOAPgdAAIgRgCgAgPhCQgHAEgDAGQgFAHgCAHQgCAIAAAJQAAAJACAJQABAHAEAHQAEAGAHAFQAGAEAKAAQAKAAAHgEQAHgFAFgGQADgHADgHQACgKAAgIIgCgQQgCgIgEgGQgEgGgHgFQgHgEgJAAQgJAAgIAEg");
	this.shape_12.setTransform(73.1,21);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#00338D").s().p("AAfA/IAAhSQAAgHgCgEQgBgEgEgDQgDgEgFgCQgEgCgGAAQgJABgGACQgHAEgFAEQgEAGgDAHQgCAHAAAIIAABFIgUAAIAAh7IATAAIAAAUIAAAAQAHgLAKgGQAKgFANgBQAMABAIADQAIADAFAGQAFAGACAIQACAJAAAKIAABQg");
	this.shape_13.setTransform(60.9,18.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#00338D").s().p("AgJBWIAAh7IATAAIAAB7gAgJg8IAAgZIATAAIAAAZg");
	this.shape_14.setTransform(52.6,16.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#00338D").s().p("AAcBWIgqhBIgSASIAAAvIgVAAIAAirIAVAAIAABlIA3g1IAbAAIgxAsIA1BPg");
	this.shape_15.setTransform(45.6,16.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#00338D").s().p("AAfA8QgEgEgBgKIgJAJQgEADgGACQgLAFgNAAQgHAAgIgCQgIgCgFgEQgGgEgDgHQgDgGAAgKQAAgKADgHQAEgHAFgEQAGgDAIgCIAPgEIAQgCIANgDQAFgBAEgDQADgDABgGQgBgHgCgEQgDgEgDgCIgJgDIgJgBQgNAAgJAFQgJAFAAAOIgVAAQABgMAFgIQAEgIAHgFQAHgEAJgDQAKgCAKAAIAQACQAIABAGADQAHAEAFAHQADAHAAAKIAAA/IABALQABADAFAAIAHgBIAAAQQgGADgJAAQgIAAgFgFgAAPACIgNACIgNACIgMAEQgGACgDAFQgEAEAAAIQAAAFACADIAGAGQADACAEABIAKABQAJAAAHgDQAHgDAFgEQAEgEACgFQACgFAAgEIAAgUQgDACgHABg");
	this.shape_16.setTransform(33.4,18.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#00338D").s().p("ABABWIAAiOIAAAAIg2COIgSAAIg2iOIAAAAIAACOIgXAAIAAirIAgAAIA1CPIA2iPIAfAAIAACrg");
	this.shape_17.setTransform(17.2,16.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txtgood, new cjs.Rectangle(4.7,0,236.1,32), null);


(lib.textbetter = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00338D").s().p("AgHApIgIgDQgIgDgFgGIgFgFIgEgHIgDgJIgBgIQABgIADgIIAEgGIAFgGQAFgGAIgDQAIgDAIAAQAIAAAHADQAIADAGAGQAFAFAEAHQACAIAAAIIAAAIIgCAJQgEAHgFAFQgGAGgIADIgHADIgIABIgIgBgAgFgiIgHADQgGACgFAFQgEAFgDAGQgDAHAAAGQAAAIADAGIADAGIAEAFQAFAFAGADIAHACIAGAAIAGAAIAGgCQAHgDAEgFIAFgFIACgGIACgHIABgHIgBgHIgCgGQgCgGgFgFQgEgFgHgCQgFgDgHAAIgGAAgAANAYIgNgUIgHAAIAAAUIgIAAIAAgvIASAAIAIABIAEADQAFACAAAIQAAAGgEADQAAAAgBAAQAAAAgBABQAAAAgBAAQAAAAgBAAIgFACIAOAVgAgHgBIAHAAIAEgBIAFgBIACgCIABgEIAAgEIgDgDIgEgBIgDAAIgJAAg");
	this.shape.setTransform(241.8,11.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00338D").s().p("AgMANIAAgZIAZAAIAAAZg");
	this.shape_1.setTransform(229,23.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00338D").s().p("AggBAIAAh7IAUAAIAAAaIAAAAQAEgIAEgFQAEgGAFgDQALgIARABIAAAVQgNAAgIAEQgJADgEAHQgFAGgDAJQgCAKAAALIAAA3g");
	this.shape_2.setTransform(223.6,18.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00338D").s().p("AgWA8QgLgFgIgIQgHgJgEgMQgEgMAAgOQAAgNAEgMQAEgMAIgIQAIgJALgFQALgFALAAQAIAAAHACQAHACAGADQALAHAGAKQAHALACANQADAMgBAKIhcAAQAAAIACAIQADAHAEAGQAFAFAIAEQAHADAJAAQANAAAIgGQAEgDADgEIAEgLIAUAAQgEAVgOAKQgHAGgIACQgJADgLAAQgNAAgLgFgAgNgrQgHADgEAFQgFAEgCAHQgDAGgBAHIBHAAQgBgHgCgGQgDgHgFgFQgFgEgGgDQgGgDgIAAQgHAAgGADg");
	this.shape_3.setTransform(213.6,18.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#00338D").s().p("AASBQIgOAAQgFgBgDgDQgEgEgBgEQgCgGAAgJIAAhOIgVAAIAAgSIAVAAIAAglIATAAIAAAlIAZAAIAAASIgZAAIAABNIABAFQAAABABAAQAAABAAAAQAAABABAAQAAAAAAABIAFABIAIAAIAJAAIAAASg");
	this.shape_4.setTransform(204,16.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#00338D").s().p("AASBQIgOAAQgFgBgDgDQgEgEgBgEQgCgGAAgJIAAhOIgVAAIAAgSIAVAAIAAglIATAAIAAAlIAZAAIAAASIgZAAIAABNIABAFQAAABABAAQAAABAAAAQAAABABAAQAAAAAAABIAFABIAIAAIAJAAIAAASg");
	this.shape_5.setTransform(197.5,16.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#00338D").s().p("AgWA8QgLgFgIgIQgHgJgEgMQgEgMAAgOQAAgNAEgMQAEgMAIgIQAIgJALgFQALgFALAAQAIAAAHACQAHACAGADQALAHAGAKQAHALACANQADAMgBAKIhcAAQAAAIACAIQADAHAEAGQAFAFAIAEQAHADAJAAQANAAAIgGQAEgDADgEIAEgLIAUAAQgEAVgOAKQgHAGgIACQgJADgLAAQgNAAgLgFgAgNgrQgHADgEAFQgFAEgCAHQgDAGgBAHIBHAAQgBgHgCgGQgDgHgFgFQgFgEgGgDQgGgDgIAAQgHAAgGADg");
	this.shape_6.setTransform(188.7,18.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#00338D").s().p("AgHBWIgLgDQgGgCgFgEQgEgDgEgGIAAAAIAAAQIgVAAIAAiqIAVAAIAABAIAAAAQADgFAEgEQAEgDAFgCQALgGAMAAIANACQAHABAFADQALAFAHAJQAHAJADAMQAEALAAANQAAANgEAMQgDALgHAJQgHAJgLAFQgLAFgOAAgAgSgUQgHAFgEAHQgFAGgCAHQgCAJAAAIQAAAKACAIQACAIAFAHQAFAHAHADQAIAEAJAAQALAAAHgEQAHgEAEgGQAFgHABgJQACgIAAgKQAAgIgCgJQgCgHgFgGQgFgGgHgFQgHgEgJAAQgKAAgIAEg");
	this.shape_7.setTransform(176.4,16.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#00338D").s().p("AgTA/QgJgCgHgFQgIgFgEgIQgEgIgBgMIAVAAQAAAHADAEQADAFAEADQAFADAGABIALABIAKgBIAKgCQAEgCADgEQADgEAAgFQAAgIgGgEQgGgEgJgDIgTgEIgTgGQgJgDgGgHQgGgGAAgMQAAgKAEgHQAEgGAHgEQAGgFAJgCQAIgCAIAAQAKAAAJACQAIACAHAEQAHAFAEAHQAEAIABALIgUAAQgBgGgCgEQgDgEgEgCQgEgDgFgBIgJgBIgJABIgJADQgEABgCADQgDADAAAFQAAAFAEAEQAEADAGADIANADIANAEIAPAEQAHACAGADQAGAEADAGQAEAGAAAJQAAALgFAIQgFAHgHAFQgIAEgJACIgTACQgKAAgJgCg");
	this.shape_8.setTransform(158.3,18.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#00338D").s().p("AgmBWIgHgBIAAgTIAGACIAGABQAGAAAEgDQAEgDADgGIAIgUIgxh7IAXAAIAjBlIABAAIAjhlIAVAAIg2CMIgGAPQgDAGgFAEQgEAEgFACQgFACgHAAg");
	this.shape_9.setTransform(147.3,21.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#00338D").s().p("AAfA8QgFgEABgKIgJAJQgGADgFACQgKAFgNAAQgJAAgHgCQgIgCgFgEQgGgEgDgHQgDgGgBgKQABgKADgHQAEgHAGgEQAFgDAIgCIAQgEIAPgCIANgDQAFgBAEgDQAEgDAAgGQAAgHgDgEQgCgEgEgCIgJgDIgJgBQgNAAgJAFQgIAFgBAOIgUAAQAAgMAEgIQAFgIAHgFQAHgEAJgDQAKgCAKAAIAPACQAJABAGADQAIAEADAHQAFAHAAAKIAAA/IABALQABADAEAAIAGgBIAAAQQgFADgJAAQgIAAgFgFgAAPACIgNACIgNACIgMAEQgGACgDAFQgEAEAAAIQAAAFACADIAFAGQAEACAEABIAJABQALAAAFgDQAIgDAFgEQAEgEACgFQACgFAAgEIAAgUQgEACgGABg");
	this.shape_10.setTransform(136.1,18.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#00338D").s().p("AgOBWIgMgEQgKgFgHgKQgHgIgEgMQgEgLAAgOQAAgNAEgLQADgLAHgKQAIgIAKgFQALgGAOAAIAJABIALADQAGADAFAEQAFADADAGIABAAIAAhAIAUAAIAACqIgUAAIAAgRIgBAAQgFALgLAFQgKAEgNAAgAgQgUQgHAFgFAHQgEAGgCAIQgCAJAAAIQAAAJACAIQADAJAEAGQAFAHAHADQAIAEAIAAQALAAAHgEQAHgEAFgGQAEgHACgJQACgIAAgJQAAgIgCgJQgCgHgFgHQgEgGgIgFQgHgEgLAAQgJAAgHAEg");
	this.shape_11.setTransform(122.9,16.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#00338D").s().p("AgOBWIgMgEQgKgFgHgKQgHgIgEgMQgEgLAAgOQAAgNAEgLQADgLAHgKQAIgIAKgFQALgGAOAAIAJABIALADQAGADAFAEQAFADADAGIABAAIAAhAIAUAAIAACqIgUAAIAAgRIgBAAQgFALgLAFQgKAEgNAAgAgQgUQgHAFgFAHQgEAGgCAIQgCAJAAAIQAAAJACAIQADAJAEAGQAFAHAHADQAIAEAIAAQALAAAHgEQAHgEAFgGQAEgHACgJQACgIAAgJQAAgIgCgJQgCgHgFgHQgEgGgIgFQgHgEgLAAQgJAAgHAEg");
	this.shape_12.setTransform(104.1,16.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#00338D").s().p("AgNBAIgMgEQgLgFgIgJQgHgIgEgMQgEgMAAgOQAAgMAEgMQAEgMAHgJQAIgJALgFQAMgFANAAQAPAAALAFQALAFAIAJQAIAJAEAMQADAMAAAMQAAAOgDAMQgEAMgIAIQgIAJgLAFIgMAEIgOABgAgOgrQgHADgFAHQgGAGgCAIQgEAJAAAKQAAAMAEAIQACAJAGAGQAFAGAHADQAHADAHAAQAIAAAHgDQAHgDAFgGQAGgGADgJQADgIAAgMQAAgKgDgJQgDgIgGgGQgFgHgHgDQgHgDgIAAQgHAAgHADg");
	this.shape_13.setTransform(91.5,18.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#00338D").s().p("AgNBAIgMgEQgLgFgIgJQgHgIgEgMQgEgMAAgOQAAgMAEgMQAEgMAHgJQAIgJALgFQAMgFANAAQAPAAALAFQALAFAIAJQAIAJAEAMQADAMAAAMQAAAOgDAMQgEAMgIAIQgIAJgLAFIgMAEIgOABgAgOgrQgHADgFAHQgGAGgCAIQgEAJAAAKQAAAMAEAIQACAJAGAGQAFAGAHADQAHADAHAAQAIAAAHgDQAHgDAFgGQAGgGADgJQADgIAAgMQAAgKgDgJQgDgIgGgGQgFgHgHgDQgHgDgIAAQgHAAgHADg");
	this.shape_14.setTransform(78.7,18.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#00338D").s().p("AgRBXQgJgCgIgEQgHgFgEgHQgGgGAAgLIAUAAQAAAGAEAEQADAEAFACIAKAEIAJABQAKgBAIgDQAGgEAFgGQAFgGACgIQABgJAAgKIAAgIQgFALgLAGQgKAEgLAAQgOAAgLgEQgKgGgHgIQgHgJgEgLQgDgKAAgNQAAgLADgLQADgMAHgKQAHgJAKgGQAMgHAOAAQAMAAAKAGQAKAFAFAKIAAgSIAUAAIAABxQAAAPgDAMQgEALgHAIQgOAPgdAAIgRgCgAgPhCQgGAEgFAGQgEAHgDAHQgCAIAAAJQAAAJACAJQACAHAEAHQAEAGAHAFQAHAEAJAAQAKAAAHgEQAHgFAEgGQAEgHADgHQABgKAAgIIgBgQQgCgIgFgGQgDgGgHgFQgHgEgKAAQgJAAgHAEg");
	this.shape_15.setTransform(65.7,21);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#00338D").s().p("AgOBWIgMgEQgKgFgHgKQgHgIgEgMQgEgLAAgOQAAgNAEgLQADgLAHgKQAIgIAKgFQALgGAOAAIAJABIALADQAGADAFAEQAFADADAGIABAAIAAhAIAUAAIAACqIgUAAIAAgRIgBAAQgFALgLAFQgKAEgNAAgAgQgUQgHAFgFAHQgEAGgCAIQgCAJAAAIQAAAJACAIQADAJAEAGQAFAHAHADQAIAEAIAAQALAAAHgEQAHgEAFgGQAEgHACgJQACgIAAgJQAAgIgCgJQgCgHgFgHQgEgGgIgFQgHgEgLAAQgJAAgHAEg");
	this.shape_16.setTransform(47,16.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#00338D").s().p("AAfA/IAAhSQAAgHgCgEQgBgEgEgDQgDgEgFgCQgEgCgGAAQgJABgGACQgHAEgFAEQgEAGgDAHQgCAHAAAIIAABFIgUAAIAAh7IATAAIAAAUIAAAAQAHgLAKgGQAKgFANgBQAMABAIADQAIADAFAGQAFAGACAIQACAJAAAKIAABQg");
	this.shape_17.setTransform(34.6,18.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#00338D").s().p("AA2BWIgSg0IhIAAIgTA0IgYAAIBEirIAXAAIBECrgAgcAPIA4AAIgchPIAAAAg");
	this.shape_18.setTransform(21.2,16.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.textbetter, new cjs.Rectangle(11.3,0,237,32), null);


(lib.man = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.mancallcar();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.man, new cjs.Rectangle(0,0,201,90), null);


(lib.family = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.familypool();
	this.instance.parent = this;
	this.instance.setTransform(1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.family, new cjs.Rectangle(1,0,201,90), null);


// stage content:
(lib._728x90__displayad = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_78 = function() {
		timesPlayed++;
		
		if (timesPlayed == 4) {
			this.stop();
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(78).call(this.frame_78).wait(77));

	// logo
	this.instance = new lib.logo();
	this.instance.parent = this;
	this.instance.setTransform(19,26);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(155));

	// button
	this.instance_1 = new lib.Bitmap5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(408,25);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(155));

	// txt good
	this.instance_2 = new lib.txtgood();
	this.instance_2.parent = this;
	this.instance_2.setTransform(264.9,44.5,1,1,0,0,0,122.7,16);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({regX:123.2,regY:18.8,x:265.4,y:47.2,alpha:0.1},0).wait(1).to({alpha:0.2},0).wait(1).to({y:47.1,alpha:0.3},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.5},0).wait(1).to({y:47,alpha:0.6},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.8},0).wait(1).to({y:46.9,alpha:0.9},0).wait(1).to({alpha:1},0).wait(2).to({alpha:0.999},0).wait(3).to({alpha:0.998},0).wait(2).to({alpha:0.997},0).wait(3).to({alpha:0.996},0).wait(3).to({alpha:0.995},0).wait(2).to({alpha:0.994},0).wait(3).to({alpha:0.993},0).wait(3).to({alpha:0.992},0).wait(2).to({alpha:0.991},0).wait(3).to({alpha:0.99},0).wait(3).to({alpha:0.989},0).wait(3).to({alpha:0.988},0).wait(2).to({alpha:0.987},0).wait(3).to({alpha:0.986},0).wait(3).to({alpha:0.985},0).wait(2).to({alpha:0.984},0).wait(3).to({alpha:0.983},0).wait(3).to({alpha:0.982},0).wait(2).to({alpha:0.981},0).wait(3).to({alpha:0.98},0).wait(2).to({alpha:0.891},0).wait(1).to({alpha:0.802},0).wait(1).to({alpha:0.713},0).wait(1).to({alpha:0.624},0).wait(1).to({alpha:0.535},0).wait(1).to({alpha:0.445},0).wait(1).to({alpha:0.356},0).wait(1).to({alpha:0.267},0).wait(1).to({alpha:0.178},0).wait(1).to({alpha:0.089},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(79));

	// man
	this.instance_3 = new lib.man();
	this.instance_3.parent = this;
	this.instance_3.setTransform(627.5,45,1,1,0,0,0,100.5,45);
	this.instance_3.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.889},0).wait(1).to({alpha:1},0).wait(56).to({alpha:0.909},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(79));

	// txt better
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#EBEBEB").ss(1,1,1,3,true).p("AgFgDIAAAHIALAA");
	this.shape.setTransform(728.6,0.4);

	this.instance_4 = new lib.textbetter();
	this.instance_4.parent = this;
	this.instance_4.setTransform(265.6,45.8,1,1,0,0,0,123.4,16);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.instance_4}]},58).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_4}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(58).to({_off:false},0).wait(1).to({regX:129.6,regY:18.8,x:271.8,y:48.4},0).wait(1).to({y:48.3},0).wait(1).to({y:48.2},0).wait(1).to({y:48.1},0).wait(1).to({y:48},0).wait(1).to({y:47.9},0).wait(1).to({alpha:0.091},0).wait(1).to({alpha:0.182},0).wait(1).to({alpha:0.273},0).wait(1).to({alpha:0.364},0).wait(1).to({alpha:0.455},0).wait(1).to({alpha:0.545},0).wait(1).to({alpha:0.636},0).wait(1).to({alpha:0.727},0).wait(1).to({alpha:0.818},0).wait(1).to({alpha:0.909},0).wait(1).to({alpha:1},0).wait(70).to({alpha:0.9},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0},0).wait(1));

	// family
	this.instance_5 = new lib.family();
	this.instance_5.parent = this;
	this.instance_5.setTransform(627.5,45,1,1,0,0,0,100.5,45);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(59).to({_off:false},0).wait(1).to({regX:101.5,x:628.5},0).wait(85).to({alpha:0.9},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.7},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(383,44,711.3,91);
// library properties:
lib.properties = {
	width: 728,
	height: 90,
	fps: 22,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [
		{src:"images/728x90__display_ad_atlas_.png", id:"728x90__display_ad_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;